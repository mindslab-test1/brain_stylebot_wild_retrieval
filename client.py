from proto.stylebot_wild_retrieval_pb2 import PersonImageInput, UserItem, UserItemOutput, ItemInput, RetrievalResult
from proto.stylebot_wild_retrieval_pb2_grpc import StylebotRetrievalStub

import argparse
import grpc
import os
import shutil

CHUNK_SIZE = 1024 * 1024  # 1MB

class Client:
    def __init__(self, ip="114.108.173.117", port=50000):
        self.server_ip = ip
        self.server_port = port
        self.stub = StylebotRetrievalStub(
            grpc.insecure_channel(
                self.server_ip + ":" + str(self.server_port)
            )
        )

    def _generate_binary_iterator(self, binary):
        for idx in range(0, len(binary), CHUNK_SIZE):
            yield PersonImageInput(image=binary[idx:idx+CHUNK_SIZE])

    def _get_result_from_iterator(self, output_iterator):
        results = [{"image": bytearray(), "path": None} for _ in range(5)]

        for chunk in output_iterator:
            for top_idx in range(len(results)):  # top: 1 -> index: 0
                if chunk.top == top_idx:
                    results[top_idx]["image"].extend(chunk.image_part)
                    results[top_idx]["path"] = chunk.path

        for result_idx in range(len(results)):
            results[result_idx]["image"] = bytes(results[result_idx]["image"])

        return results

    def get_service(self, image_dir):
        shutil.rmtree("temp")
        os.makedirs("temp", exist_ok=True)

        with open(image_dir, 'rb') as rf:
            image_byte = rf.read()
        image_iterator = self._generate_binary_iterator(image_byte)

        grpc_output = self.stub.GetSegmentationResult(image_iterator)
        # print(len(grpc_output.items))
        for idx, output_item in enumerate(grpc_output.items):
            with open(os.path.join("temp", "result_plot_{:02d}.jpg".format(idx)), "wb") as f:
                f.write(output_item.bitmask)

        retrieval_output_list = []
        for items in grpc_output.items:
            output_iterator = self.stub.GetRetrievalResult(ItemInput(item_id=items.item_id))
            retrieval_output = self._get_result_from_iterator(output_iterator)
            for i in range(len(retrieval_output)):
                print(retrieval_output[i]["path"], f"{len(retrieval_output[i]['image'])} bytes image requested")

            retrieval_output_list.append(retrieval_output)

        return retrieval_output_list


if __name__ == '__main__':
    image_path = 'test/021.png'
    client = Client()
    _ = client.get_service(image_path)
