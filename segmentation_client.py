from proto.segmentation_pb2 import Input, Point, BoundingBox, SegItem, SegResult
from proto.segmentation_pb2_grpc import ImageSegStub

import argparse
import grpc
import numpy as np
import cv2

CHUNK_SIZE = 1024 * 1024  # 1MB


class SegmnetationClient:
    def __init__(self, remote):
        self.channel = grpc.insecure_channel(remote)
        self.stub = ImageSegStub(self.channel)

    def _generate_binary_iterator(self, binary):
        for idx in range(0, len(binary), CHUNK_SIZE):
            yield Input(image=binary[idx:idx + CHUNK_SIZE])

    def get_service_with_filename(self, image_dir):
        h, w = cv2.imread(image_dir, cv2.IMREAD_COLOR).shape[:2]
        with open(image_dir, 'rb') as rf:
            image_binary = rf.read()
        grpc_output = self.get_service(image_binary)
        return grpc_output

    def get_service(self, image_binary):
        image_iterator = self._generate_binary_iterator(image_binary)
        grpc_output = self.stub.Determinate(image_iterator)
        grpc_filtered_output = self._filter_item(grpc_output)
        grpc_bbox_refined_output = self._refine_bbox(grpc_filtered_output)
        return grpc_bbox_refined_output

    @staticmethod
    def _filter_item(grpc_item_list):
        clothes_item = [grpc_item for grpc_item in grpc_item_list.items
                        if grpc_item.confidence > 0.4 and grpc_item.class_id in range(0, 13)]

        return SegResult(items=clothes_item)

    @staticmethod
    def _refine_bbox(grpc_item_list):
        sleeve_ids = [0, 1, 2, 3, 4, 9, 10]
        sleeve_items = [grpc_item for grpc_item in grpc_item_list.items if grpc_item.class_id in sleeve_ids]
        non_sleeve_items = [grpc_item for grpc_item in grpc_item_list.items if grpc_item.class_id not in sleeve_ids]

        # get min/max x value for bboxes
        bbox_width_list = np.array([[grpc_item.box.left_top.x, grpc_item.box.left_top.y,
                                     grpc_item.box.right_bottom.x, grpc_item.box.right_bottom.y]
                                    for grpc_item in sleeve_items])
        x_min = np.amin(bbox_width_list[:, 0])
        y_min = np.amin(bbox_width_list[:, 1])
        x_max = np.amax(bbox_width_list[:, 2])
        y_max = np.amax(bbox_width_list[:, 3])
        width = x_max - x_min
        height = y_max - y_min

        for item_idx in range(len(sleeve_items)):
            item_width = sleeve_items[item_idx].box.right_bottom.x - sleeve_items[item_idx].box.left_top.x
            item_height = sleeve_items[item_idx].box.right_bottom.y - sleeve_items[item_idx].box.left_top.y
            if item_width / float(width) < 0.6:
                sleeve_items[item_idx].box.right_bottom.x = x_max
                sleeve_items[item_idx].box.left_top.x = x_min
            if item_height / float(height) < 0.3:
                sleeve_items[item_idx].box.right_bottom.y = y_max
                sleeve_items[item_idx].box.left_top.y = y_min

        clothes_item = sleeve_items + non_sleeve_items
        return SegResult(items=clothes_item)

    def close_channel(self):
        self.channel.close()


if __name__ == '__main__':
    image_path = 'sample/sample.jpg'
    client = SegmnetationClient()
    print(client.get_service_with_filename(image_path))
