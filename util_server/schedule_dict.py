import os
import time
import csv
import logging
from datetime import datetime
import threading

logger = logging.getLogger(name='stylebot_retrieval')
job_class_lock = threading.RLock()


def get_time_difference(strftime_now, strftime_old):
    FMT = '%Y-%m-%d %H:%M:%S'
    tdelta = datetime.strptime(strftime_now, FMT) - datetime.strptime(strftime_old, FMT)
    return tdelta.total_seconds()


class ScheduleDict:
    def __init__(self):
        self.job_scheduler = dict()
        self.order = 0
        os.makedirs("logs", exist_ok=True)

    def add_element(self, request_key, return_val=True, **kwargs):
        with job_class_lock:
            self.job_scheduler[request_key] = {"created_at": None, "done_at": None, "clothes_item": None, "image": None}
            self.order += 1

            try:
                for key, value in kwargs.items():
                    self.job_scheduler[request_key][key] = value
            except KeyError as e:
                raise e

            if return_val:
                return self.job_scheduler[request_key]

    def get_element(self, request_key, *args):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
            except KeyError:
                return None

            if args is None:
                return job_elem
            else:
                return_val = [job_elem[key] for key in args]
                return return_val

    def remove_element(self, request_key, return_val=True):
        with job_class_lock:
            try:
                popped_elem = self.job_scheduler.pop(request_key)
            except KeyError as e:
                raise e

            if return_val:
                return popped_elem

    def put_element(self, request_key, **kwargs):
        with job_class_lock:
            try:
                for key, value in kwargs.items():
                    self.job_scheduler[request_key][key] = value
            except KeyError as e:
                raise e

    def is_exist(self, request_key):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
                if job_elem:
                    return True
                else:
                    return False
            except KeyError:
                return False

    def _get_key_list(self):
        with job_class_lock:
            return list(self.job_scheduler.keys())

    def __iter__(self):
        with job_class_lock:
            return iter(list(self.job_scheduler.items()))

    def __str__(self):
        with job_class_lock:
            if len(self.job_scheduler) != 0:
                return "\t".join(["{:s}".format(key) for key in self.job_scheduler.keys()])
            else:
                return "No Process"

    def __len__(self):
        with job_class_lock:
            return len(self.job_scheduler)
