from proto.stylebot_kp_pb2 import Point, BoundingBox, ClothesItemInput, SegItem
from proto.stylebot_kp_pb2_grpc import KPEstimatorStub

import argparse
import grpc
import numpy as np
import cv2

CHUNK_SIZE = 1024 * 1024  # 1MB
SUPPORTING_TYPES = [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]


class KPEstimatorClient:
    def __init__(self, remote):
        self.channel = grpc.insecure_channel(remote)
        self.stub = KPEstimatorStub(self.channel)

    def _generate_binary_iterator(self, binary, clothes_item):
        for idx in range(0, len(binary), CHUNK_SIZE):
            yield ClothesItemInput(image=binary[idx:idx + CHUNK_SIZE])

        if clothes_item is not None:
            yield ClothesItemInput(items=clothes_item)

    def get_service_with_filename(self, image_dir, item_list=None):
        # h, w =  cv2.imread(image_dir, cv2.IMREAD_COLOR).shape[:2]
        with open(image_dir, 'rb') as rf:
            image_byte = rf.read()
        grpc_output = self.get_service(image_byte, item_list)
        return grpc_output

    def get_service(self, image_binary, clothes_item):

        if clothes_item.class_id not in SUPPORTING_TYPES:
            return None

        kp_box = BoundingBox(left_top=Point(x=clothes_item.box.left_top.x,
                                            y=clothes_item.box.left_top.y),
                             right_bottom=Point(x=clothes_item.box.right_bottom.x,
                                                y=clothes_item.box.right_bottom.y))
        kp_clothes_item = SegItem(class_id=clothes_item.class_id,
                                  bitmask=clothes_item.bitmask,
                                  box=kp_box,
                                  confidence=clothes_item.confidence)
        grpc_output = self.stub.Determinate(ClothesItemInput(image=image_binary, seg_item=kp_clothes_item))
        return grpc_output

    def close_channel(self):
        self.channel.close()


if __name__ == '__main__':
    image_path = 'sample/sample.jpg'
    client = KPEstimatorClient()
    print(client.get_service_with_filename(image_path))
