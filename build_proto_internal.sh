python -m grpc.tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ proto/segmentation.proto
python -m grpc.tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ proto/stylebot_kp.proto
python -m grpc.tools.protoc --proto_path=./ --python_out=./ --grpc_python_out=./ proto/image_retrieval.proto
