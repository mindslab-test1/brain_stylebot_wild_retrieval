FROM docker.maum.ai:443/brain/vision:cu101-torch160
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR.UTF-8
EXPOSE 50000

COPY . /stylebot_ret
WORKDIR /stylebot_ret

RUN pip install -r requirements.txt