# -*- coding: utf-8 -*-
from util_server.schedule_dict import get_time_difference, ScheduleDict
from retrieval_client import ImageRetrievalClient
from kp_estimator_client import KPEstimatorClient
from segmentation_client import SegmnetationClient
from proto.stylebot_wild_retrieval_pb2 import PersonImageInput, UserItem, UserItemOutput, ItemInput, RetrievalResult
from proto.stylebot_wild_retrieval_pb2_grpc import add_StylebotRetrievalServicer_to_server, StylebotRetrievalServicer
import logging
import argparse
import grpc
import time
import os
import shutil
from datetime import datetime
import uuid
import numpy as np
import cv2
import PIL.Image as Image
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont
from concurrent import futures

import sys

sys.path.insert(0, "./proto")


_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_CHUNK_SIZE = 1024 * 1024
_REST_TIMEOUT = 60 * 5  # 300 secs
_FMT = '%Y-%m-%d %H:%M:%S'

scheduler = ScheduleDict()

category_names = [
    'short_sleeved_shirt',
    'long_sleeved_shirt',
    'short_sleeved_outwear',
    'long_sleeved_outwear',
    'vest',
    'sling',
    'shorts',
    'trousers',
    'skirt',
    'short_sleeved_dress',
    'long_sleeved_dress',
    'vest_dress',
    'sling_dress'
]


class StylebotRetrievalRunner:
    def __init__(self):
        self.logger = logging.getLogger('stylebot_retrieval')

    def run(self):
        self.logger.info(scheduler)

        try:
            self._clean_scheduler()
        except RuntimeError as e:
            self.logger.error("{:s}".format(str(e)))
        except KeyError as e:
            self.logger.error("{:s}".format(str(e)))

    @staticmethod
    def _clean_scheduler():
        strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)

        remove_keys = set()
        timeout_keys = []
        for _key, _elem in scheduler:
            waiting_time = get_time_difference(strftime_now, _elem["create_at"])
            if waiting_time > _REST_TIMEOUT:
                timeout_keys.append(_key)

        remove_keys.update(timeout_keys)

        for _key in remove_keys:
            scheduler.remove_element(_key, return_val=False)


class StylebotRetrievalServicerImpl(StylebotRetrievalServicer):
    def __init__(self, segmentation_port, kp_estimator_port, image_retrieval_port,
                 server_temp_dir="server_tmp", plot_debug=False):
        self.segmentation_port = segmentation_port
        self.kp_estimator_port = kp_estimator_port
        if image_retrieval_port.upper() != "NONE":
            self.image_retrieval_port = image_retrieval_port
        else:
            self.image_retrieval_port = None

        self.temp_dir = server_temp_dir
        self.logger = logging.getLogger('stylebot_retrieval')
        self.plot_debug = plot_debug

    def _get_segmentation_result(self, image_binary):
        segmentation_client = SegmnetationClient(self.segmentation_port)
        try:
            items = segmentation_client.get_service(image_binary)
            segmentation_client.close_channel()
            return items
        except Exception as e:
            segmentation_client.close_channel()
            raise e

    def _get_keypoint_result(self, image_binary, item_list):
        kp_estimator_client = KPEstimatorClient(self.kp_estimator_port)
        try:
            warped_item_list = []
            print(f'request:{len(image_binary)} byte')
            print(f'request:{len(item_list.items)} items')
            for clothes_item in item_list.items:
                warped_item = kp_estimator_client.get_service(image_binary, clothes_item)
                if warped_item is None:
                    warped_item_list.append(None)
                else:
                    warped_item_list.append(warped_item)
            kp_estimator_client.close_channel()
            return warped_item_list
        except Exception as e:
            kp_estimator_client.close_channel()
            raise e

    def _get_retrieval_result(self, warped_image_binary, warped_mask_binary, class_id):
        if self.image_retrieval_port is not None:
            image_retrieval_client = ImageRetrievalClient(self.image_retrieval_port)
            try:
                retreival_result = image_retrieval_client.get_service(warped_image_binary, warped_mask_binary, class_id)
                image_retrieval_client.close_channel()
                return retreival_result

            except Exception as e:
                image_retrieval_client.close_channel()
                raise e
        else:
            results = [{"image": None, "path": None} for _ in range(5)]

            results[0]["image"] = warped_image_binary
            results[1]["image"] = warped_image_binary
            results[2]["image"] = warped_image_binary
            results[3]["image"] = warped_image_binary
            results[4]["image"] = warped_image_binary

            results[0]["path"] = "BLANK1"
            results[1]["path"] = "BLANK2"
            results[2]["path"] = "BLANK3"
            results[3]["path"] = "BLANK4"
            results[4]["path"] = "BLANK5"

            return results

    def GetSegmentationResult(self, image_iterator, context):
        try:
            wild_image_data = bytearray()
            for input_data in image_iterator:
                if input_data.image is not None:
                    wild_image_data.extend(input_data.image)

            wild_image_data = bytes(wild_image_data)

            if len(wild_image_data) > 1000 * 1000 * 10:
                raise AssertionError("Image is too big.")

            item_list = self._get_segmentation_result(wild_image_data)
            warped_item_list = self._get_keypoint_result(wild_image_data, item_list)
            if len(warped_item_list) == 0:
                return None  # There is no item detected
            else:
                print(f"{len(warped_item_list)} items exist!!!")

            output_list = []
            if os.path.exists(self.temp_dir):
                shutil.rmtree(self.temp_dir)
            os.makedirs(self.temp_dir, exist_ok=True)
            for item_idx, (seg_clothes, warped_clothes) in enumerate(zip(item_list.items, warped_item_list)):
                request_key = str(uuid.uuid4())

                strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
                scheduler.add_element(request_key, create_at=strftime_now, clothes_item=warped_clothes,
                                      image=wild_image_data)

                if self.plot_debug:
                    plot_result = self._get_visulization_output(wild_image_data, seg_clothes)
                else:
                    plot_result = None

                user_item = self._convert_output_item(request_key, seg_clothes, plot_result)
                output_list.append(user_item)

            return UserItemOutput(items=output_list)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetRetrievalResult(self, item_input, context):
        try:
            warped_item = scheduler.get_element(item_input.item_id, 'clothes_item')[0]
            retrieval_result = self._get_retrieval_result(warped_item.clothes_image, warped_item.bitmask,
                                                          warped_item.class_id)
            retrieval_iterator = self._convert_retrieval_result(retrieval_result)
            return retrieval_iterator

        except KeyError as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @staticmethod
    def _convert_output_item(item_key, segmentation_item, plot_result=None):
        if plot_result is not None:
            user_item = UserItem(item_id=item_key,
                                 class_id=segmentation_item.class_id,
                                 bitmask=plot_result)
        else:
            user_item = UserItem(item_id=item_key,
                                 class_id=segmentation_item.class_id,
                                 bitmask=segmentation_item.bitmask)

        return user_item

    @staticmethod
    def _convert_retrieval_result(retrieval_result):

        for top_idx, retrieval_item in enumerate(retrieval_result, start=0):  # top: 1 -> index: 0
            for idx in range(0, len(retrieval_item["image"]), _CHUNK_SIZE):
                yield RetrievalResult(image_part=retrieval_item["image"][idx:idx + _CHUNK_SIZE],
                                      top=top_idx)
            yield RetrievalResult(path=retrieval_item["path"], top=top_idx)

    @staticmethod
    def _get_visulization_output(image_binary, segmentation_item):
        """
        Added for AI Consultant's request: plot items individually with white background
        """
        image = cv2.imdecode(np.frombuffer(image_binary, dtype=np.uint8), cv2.IMREAD_COLOR)
        h, w = image.shape[:2]
        mask_image = cv2.imdecode(np.frombuffer(segmentation_item.bitmask, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)
        mask_bool_array = mask_image > 127

        image = image.astype(np.int32)
        image[np.logical_not(mask_bool_array), :] = np.clip(image[np.logical_not(mask_bool_array), :] - 127, 0, 255)
        image = image.astype(np.uint8)
        image = image[..., ::-1]  # BGR to RGB
        im = Image.fromarray(image)
        draw = ImageDraw.Draw(im)
        font = ImageFont.truetype('NanumGothic.ttf', 20)
        draw.text((20, 20), "{:s}_{:.04f}".format(category_names[segmentation_item.class_id],
                                                  segmentation_item.confidence),
                  fill="white", font=font, align="left")
        draw.rectangle([max(segmentation_item.box.left_top.x - 30, 0),
                        max(segmentation_item.box.left_top.y - 30, 0),
                        min(segmentation_item.box.right_bottom.x + 30, w),
                        min(segmentation_item.box.right_bottom.y + 30, h)],
                       width=3)

        image = np.array(im)[..., ::-1]  # RGB to BGR

        image_binary = cv2.imencode(".jpg", image)[1].tostring()
        return image_binary


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Retrieval Result (work with Wav2Lip and TTS)')
    parser.add_argument("--port", type=int, default=50000, help='grpc port for lip sync servicer')
    parser.add_argument('--segmentation_remote',
                        help='segmentation grpc: ip:port',
                        type=str,
                        default='127.0.0.1:50100')
    parser.add_argument('--kps_warp_remote',
                        help='warping with keypoint grpc: ip:port',
                        type=str,
                        default='127.0.0.1:50200')
    parser.add_argument('--retrieval_remote',
                        help='image retrieval grpc: ip:port',
                        type=str,
                        default='127.0.0.1:50300')
    parser.add_argument('--plot_debug', help='get visulziation output instead of bitmask', action="store_true")
    parser.add_argument('--log_level', help='logger level', type=str, default='INFO')
    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    os.makedirs("logs", exist_ok=True)

    lipsync_logger = logging.getLogger('stylebot_retrieval')
    file_handler = logging.FileHandler('./logs/log_server.log')
    lipsync_logger.addHandler(file_handler)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), )
    lip_sync_servicer = StylebotRetrievalServicerImpl(args.segmentation_remote,
                                                      args.kps_warp_remote,
                                                      args.retrieval_remote,
                                                      plot_debug=args.plot_debug)
    add_StylebotRetrievalServicer_to_server(lip_sync_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    runner = StylebotRetrievalRunner()
    lipsync_logger.info('StylebotPartRetrieval starting at 0.0.0.0:%d', args.port)
    try:
        while True:
            runner.run()
            time.sleep(10)
    except KeyboardInterrupt:
        server.stop(0)
