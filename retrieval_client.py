from proto.image_retrieval_pb2 import InputItem, RetrievalResult
from proto.image_retrieval_pb2_grpc import ImageRetrievalStub

import argparse
import grpc
import numpy as np
import cv2

CHUNK_SIZE = 1024 * 1024  # 1MB
SUPPORTING_TYPES = [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

class ImageRetrievalClient:
    def __init__(self, remote):
        self.channel = grpc.insecure_channel(remote)
        self.stub = ImageRetrievalStub(self.channel)

    def _generate_binary_iterator(self, image_binary, mask_binary, class_id):

        yield InputItem(class_id=class_id)
        for idx in range(0, len(image_binary), CHUNK_SIZE):
            yield InputItem(image=image_binary[idx:idx + CHUNK_SIZE])
        for idx in range(0, len(mask_binary), CHUNK_SIZE):
            yield InputItem(mask=mask_binary[idx:idx + CHUNK_SIZE])

    def get_service(self, image_binary, mask_binary, class_id):
        input_iterator = self._generate_binary_iterator(image_binary, mask_binary, class_id)
        output_iterator = self.stub.Retrieval(input_iterator)

        results = [{"image": bytearray(), "path": None} for _ in range(5)]

        for chunk in output_iterator:
            if chunk.top is not None:
                top = chunk.top

            if chunk.image_part is not None:
                results[top - 1]['image'].extend(chunk.image_part)

            if chunk.path is not None:
                results[top - 1]['path'] = chunk.path

        for result_idx in range(len(results)):
            results[result_idx]["image"] = bytes(results[result_idx]["image"])

        return results

    def close_channel(self):
        self.channel.close()
